/**
 * Funcion para sumar tres numeros
 * @param {*} numero1 
 * @param {*} numero2 
 * @param {*} numero3 
 * @returns suma
 */
// ARGUMENTOS CON VALORES POR DEFECTO
// Si ponemos los numero=0, si solo le pasas un numero la funcion sigue funcionando
// si no lo pones, obligatoriamente tienes que pasar tres numeros
function uno(numero1, numero2 = 0, numero3 = 0) {
    return numero1 + numero2 + numero3;
}

/**
 * Calcula el producto de dos numeros
 * @param {*} numero1 
 * @param {*} numero2 
 * @returns producto
 */
function producto(numero1, numero2) {
    let resultado = 0; // variable local
    resultado = numero1 * numero2;
    return resultado;
}

/**
 * Sumar todos los numeros del array
 * @param {array} numeros 
 * @returns suma del array
 */
function sumarArray(numeros) {
    let resultado = 0;

    //Con el metodo foreach del array
    numeros.forEach(function (valor, indice) {
        resultado += valor;
    });
    // Lo mismo con funcion arrow
    // numeros.forEach((valor, indice) => {
    //     resultado += valor;
    // })

    // Con un for
    // for (let i = 0; i < numeros.length; i++) {
    //     resultado += numeros[i];
    // }
    return resultado;
}

/**
 * Sumar los numeros positivos del array
 * @param {array} numeros 
 * @returns suma de los numeros positivos
 */
function sumaPositivosArray(numeros) {
    let suma = 0;
    //CON UN FOREACH
    numeros.forEach(function (valor) {
        if (valor > 0) {
            suma += valor;
        }
    });

    // //CON UN FOR
    // for (let i = 0; i < numeros.length; i++) {
    //     if (numeros[i] > 0) {
    //         suma += numeros[i]
    //     }
    // }
    return suma;
}


/**
 * Sumar los numeros pares del array
 * @param {array} numeros 
 * @returns suma de los numeros pares
 */
function sumaParesArray(numeros) {
    let suma = 0;
    //CON UN FOREACH
    numeros.forEach(function (valor) {
        if (valor % 2 == 0) {
            suma += valor;
        }
    });

    // //CON UN FOR
    // for (let i = 0; i < numeros.length; i++) {
    //     if (numeros[i] %2== 0) {
    //         suma += numeros[i]
    //     }
    // }
    return suma;
}

/**
 * Funcion que calcula el numero de Aes que tiene un texto
 * @param {*} texto 
 * @returns número de Aes
 */
function contarAes(texto) {
    let salida = 0;
    //CON UN FOREACH
    // convertimos el string a un array. Los strings no tienem el metodo foreach
    let cadena = texto.split("");
    cadena.forEach(function (caracter) {
        if (caracter.toLowerCase() == "a") {
            salida++;
        }
    });

    //CON UN FOR
    // for (let i = 0; i < texto.length; i++) {
    //     if (texto[i].toLowerCase() == "a") {
    //         salida++;
    //     }
    // }
    return salida;
}