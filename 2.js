function convertir() {
    // nombre => nombre introducido por el usuario en el formulario
    let nombre = document.querySelector('#nombre').value;
    // apellidos => apellidos introducido por el usuario en el formulario
    let apellidos = document.querySelector('#apellidos').value;

    // funcion concatenar para concatenar el nombre y los apellidos
    let nombreEntero = concatenar([nombre, apellidos]);
    // funcion concatenar para concatenar las iniciales del nombre y del apellido
    let iniciales = concatenar([nombre[0], ".", apellidos[0], "."]);

    // rellanamos el HTML con los valores calculados
    document.querySelector('.nombreEntero').innerHTML = nombreEntero;
    document.querySelector('.iniciales').innerHTML = iniciales;

}

/**
 * Funcion que concatena todos los textos pasados en el array
 * @param {array} textos array con textos
 * @returns texto concatenado
 */
function concatenar(textos) {
    let salida = "";

    textos.forEach(function (valor, indice) {
        salida += " " + valor;
    });

    // for (let i = 0; i < textos.length; i++) {
    //     salida += textos[i];
    // }

    return salida;
}