//Cuando se hace clic en el boton sumar números
function sumar() {
    // Leo los números introducidos en el input
    let numeros = document.querySelector('#numeros').value;
    // Convertir numeros de string a array
    numeros = numeros.split(",");
    // Calculamos la suma llamando a la funcioón sumarArray
    let suma = sumarArray(numeros);
    // Modificamos el contenido del div resultado HTML
    document.querySelector('.resultado').innerHTML = "La suma es: " + suma;
}

//Cuando se hace clic en el boton sumar pares
function sumarPares() {
    let numeros = document.querySelector('#numeros').value;
    numeros = numeros.split(",");

    let sumaPares = sumaParesArray(numeros);

    document.querySelector('.resultado').innerHTML = "La suma de los números pares es: " + sumaPares;
}

//Cuando se hace clic en el boton sumar positivos
function sumarPositivos() {
    let numeros = document.querySelector('#numeros').value;
    numeros = numeros.split(",");

    let sumaPos = sumaPositivosArray(numeros);

    document.querySelector('.resultado').innerHTML = "La suma de los números positivos es: " + sumaPos;
}


//  Lo hacemos solo con una función 

function operaciones(tipo) {
    let numeros = document.querySelector('#numeros').value;
    numeros = numeros.split(",");

    let suma = 0;
    switch (tipo) {
        case 'suma':
            suma = sumarArray(numeros);
            document.querySelector('.resultado').innerHTML = "La suma de los números es: " + suma;
            break;
        case 'sumaPares':
            suma = sumaParesArray(numeros);
            document.querySelector('.resultado').innerHTML = "La suma de los números pares es: " + suma;
            break;
        case 'sumaPositivos':
            suma = sumaPositivosArray(numeros);
            document.querySelector('.resultado').innerHTML = "La suma de los números positivos es: " + suma;
            break;
        default:
            break;
    }
}

/**
 * Sumar todos los numeros del array
 * @param {array} numeros 
 * @returns suma del array
 */
function sumarArray(numeros) {
    let resultado = 0;

    numeros.forEach(function (valor) {
        resultado += parseInt(valor);
    });
    return resultado;
}


/**
 * Sumar los numeros pares del array
 * @param {array} numeros 
 * @returns suma de los numeros pares
 */
function sumaParesArray(numeros) {
    let suma = 0;

    numeros.forEach(function (valor) {
        if (valor % 2 == 0) {
            suma += parseInt(valor);
        }
    });
    return suma;
}

/**
 * Sumar los numeros positivos del array
 * @param {array} numeros 
 * @returns suma de los numeros positivos
 */
function sumaPositivosArray(numeros) {
    let suma = 0;

    numeros.forEach(function (valor) {
        if (valor > 0) {
            suma += parseInt(valor);
        }
    });
    return suma;
}
